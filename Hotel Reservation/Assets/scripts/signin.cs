﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class signin : MonoBehaviour {
	InputField username,email,password,country;
	InputField phone,dob,age;
	Button register;

	Person tempPerson;

	void Start () {

		tempPerson = new Person ();

		username = GameObject.Find ("nameRegister").GetComponent<InputField> ();
		email = GameObject.Find ("emailRegister").GetComponent<InputField> ();
		password = GameObject.Find ("passwordRegister").GetComponent<InputField> ();
		country = GameObject.Find ("countryRegister").GetComponent<InputField> ();
		phone = GameObject.Find ("phoneRegister").GetComponent<InputField> ();
		dob = GameObject.Find ("DOBRegister").GetComponent<InputField> ();
		age = GameObject.Find ("ageRegister").GetComponent<InputField> ();
		register = GameObject.Find ("RegisterButton").GetComponent<Button> ();

		register.onClick.AddListener (Register);
	}

	private void Register(){
		tempPerson.username = username.text;
		tempPerson.password = password.text;
		tempPerson.age = int.Parse (age.text);
		tempPerson.country = country.text;
		tempPerson.phone = int.Parse (phone.text);
		tempPerson.email = email.text;
		tempPerson.dob = int.Parse (dob.text);
		GameObject.Find ("dataStore").GetComponent<dataStore> ().allPersons.Add (tempPerson);
		SceneManager.LoadScene ("menu");
	}

}
