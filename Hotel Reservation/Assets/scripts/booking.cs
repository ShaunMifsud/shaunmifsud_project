﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.IO;

public class booking : MonoBehaviour {
	InputField fromDate,till,noPeople,roomNumber,vehicles,notes;
	Button  checkinButton;
	Text numberText,vehiclesText,resultText,roomText;
	int numberOfPeople,numberOfvehicles,room;

	public List<string> bookings;

	void Start () {
		bookings = new List<string> ();

		fromDate = GameObject.Find ("fromField").GetComponent<InputField> ();
		till = GameObject.Find ("tillField").GetComponent<InputField> ();
		noPeople = GameObject.Find ("noOfPeopleField").GetComponent<InputField> ();
		roomNumber = GameObject.Find ("roomNumberField").GetComponent<InputField> ();
		vehicles = GameObject.Find ("vehiclesField").GetComponent<InputField> ();
		notes = GameObject.Find ("notesField").GetComponent<InputField> ();
		checkinButton = GameObject.Find ("checkInButton").GetComponent<Button> ();
		numberText= GameObject.Find ("numberText").GetComponent<Text> ();
		vehiclesText= GameObject.Find ("vehiclesText").GetComponent<Text> ();
		resultText= GameObject.Find ("resultText").GetComponent<Text> ();
		roomText= GameObject.Find ("roomText").GetComponent<Text> ();

		checkinButton.onClick.AddListener (enterBookings);
	}
	

	private void enterBookings () {
		try{
			numberOfvehicles=0;
			numberOfvehicles = int.Parse (vehicles.text);
			vehiclesText.text="";
		}
		catch(Exception e){
			vehiclesText.text="only numbers than 0 are accepted";
		}

		try{
			numberOfPeople=0;
			numberOfPeople = int.Parse (noPeople.text);
			numberText.text="";
		}
		catch(Exception e){
			numberText.text="only numbers greater than 0 are accepted";
		}

		try{
			room=0;
			room = int.Parse (roomNumber.text);
			roomText.text="";
		}
		catch(Exception e){
			roomText.text="only numbers than 0 are accepted";
		}


		if (fromDate.text == "" || till.text == "" || numberOfPeople < 0 | room < 0) {
			resultText.text = ("All fields are requiered except notes or no. vehicles");
		} 
		else {
			//store booking information
			//...

			//resultText.text = "Booking Succesfull";
		}
	
		
	}
}
